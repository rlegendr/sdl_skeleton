#include "../includes/skeleton.h"

using namespace std;

void	runner_loop(t_sdl *sdl, const Uint8 *state)
{
	(void)sdl;
	while (1)
	{
		state = SDL_GetKeyboardState(NULL);
		SDL_PumpEvents();
		if (state[SDL_SCANCODE_ESCAPE])
			break ;
	}
}


int		main()
{
	t_sdl		sdl;
	const Uint8	*state;

	SDL_init_struct(&sdl);
	SDL_init_window(&sdl);
	SDL_init_renderer(&sdl);
	SDL_init_img();
	runner_loop(&sdl, state);
	SDL_clean_struct(&sdl);
	return (0);
}
